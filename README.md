https://youtu.be/Nv2DERaMx-4


Modules
1. Quickstart
2. Maven
3. Spring Framework & Boot
4. Dependency Injection
5. Configuration
6. Databases Pt 1 --Database Basics
7. Databases Pt 2 -- DAOs with Spring JDBC
8. Databases Pt 3 -- Spring Data JPA
9. Jackson & JSON
10. Building a REST API
11. Deployment to AWS LightSail

Links
🔗 Source Code: https://github.com/devtiro/course-spring-boot.git


## Timestamps
    00:00:00 Intro
### Module 1: Quickstart
    00:00:46 1.1. Building the Quickstart App
    00:07:24 1.2. Quickstart App Explainer
### Module 2: Maven
    00:11:15 2.1. What is Maven?
    00:12:56 2.2. Maven Concepts
    00:19:43 2.3. Maven Project Structure
    00:23:20 2.4. Maven Workflow
    00:26:52 2.5. Maven Spring Boot Plugin
### Module 3: Spring Framework & Boot
    00:27:59 3.1. Spring Framework vs Spring Boot
    00:29:55 3.2. Spring App Layers
    00:33:14 3.3. Modularity
### Module 4: Dependency Injection
    00:34:52 4.1. Inversion of Control
    00:37:39 4.2. Introducing Beans
    00:49:39 4.3. @Component and Friends
    00:54:18 4.4. Component Scanning
    00:59:28 4.5. @SpringBootApplication Annotation
    01:01:22 4.6. Autoconfiguration
### Module 5: Configuration
    01:05:55 5.1. Configuration files
    01:11:27 5.2. Environment Variables
    01:16:02 5.3 Configuration Properties
### Module 6: Databases Part 1 - Basics
    01:21:47 6.1. Database Layers
    01:24:51 6.2. Connect to a H2 Database
    01:29:29 6.3. Connect to a PostgreSQL Database
    01:34:58 6.4. Initialise DB Schema
### Module 7: Databases Part 2 - Spring JDBC (SQL)
    01:38:48 7.1. JDBCTemplate Setup
    01:40:44 7.2. What is a DAO?
    01:42:46 7.3. Set Up a DAO
    01:53:21 7.4. Create DAOs
    02:06:41 7.5. Implement Read One
    02:21:25 7.6. Create Integration Test
    02:34:49 7.7. Find Many
    02:48:42 7.8. Full Update
    02:58:37 7.9. Implement Delete
### Module 8: Databases Part 3 - Spring Data JPA (Objects)
    03:05:14 8.1. Spring Data JPA Setup
    03:09:43 8.2. Create Entities
    03:14:32 8.3. Hibernate Auto DDL
    03:17:48 8.4 Implement Create & Read
    03:24:58 8.5 Implement Find All
    03:26:56 8.6 Implement Update
    03:28:31 8.7 Implement Delete
    03:30:37 8.8 Custom Queries
    03:34:31 8.9 HQL
### Module 9: Jackson & JSON
    03:38:16 9.1 What is Jackson?
    03:38:54 9.1 Jackson & Spring Web
    03:42:24 9.3 Java Objects to JSON
    03:45:24 9.4 JSON to Java Objects
    03:47:36 9.5 Renaming JSON Properties
    03:49:11 9.6 Ignore Properties
### Module 10: Build a REST API
    03:51:13 10.1 REST API Design
    03:57:46 10.2 Author Create Endpoint
    04:15:06 10.3 Test Author Create Endpoint
    04:23:41 10.4 Book Create Endpoint
    04:39:26 10.5 Author List Endpoint
    04:46:31 10.6 Book List Endpoint
    04:51:57 10.7 Author Read One Endpoint
    04:58:26 10.8 Book Read One Endpoint
    05:02:50 10.9 Author Full Update Endpoint
    05:13:58 10.10 Book Full Update Endpoint
    05:22:40 10.11 Author Partial Update Endpoint
    05:31:39 10.12 Book Partial Update Endpoint
    05:40:04 10.13 Author Delete Endpoint
    05:44:52 10.14 Book Delete Endpoint
    05:48:44 10.15 Nested Objects
    05:54:32 10.16 Pagination
### Module 11: Deployment
    06:01:15  Deployment to AWS LightSail
    06:13:23 Next Up


-----------------------------------------------------------------------
1) maven clean phase 
```shell
./mvnw clean
```

2) maven default phase
````shell
./mvnw compile
````
````shell
./mvnw test
````
````shell
./mvnw package
````
````shell
./mvnw verify
````



Introduction to the Standard Directory Layout
https://maven.apache.org/guides/introduction/introduction-to-the-standard-directory-layout.html

**to run the application like as in production**
````shell
./mvnw clean verify
````
````shell
java -jar .\target\quickstart-0.0.1-SNAPSHOT.jar
````

**to run the application with spring boot maven plugin**
````shell
./mvnw spring-boot:run
````


### Spring App Layers
* **Presentation** = Controllers (rest api, graphql, websockets...)
* **Service**
* **Persistence** (handle all the interaction with persistence technologies like DB's)


### 6-databases:

    Spring Data JPA -> JPA, hibernate (ORM)
    (handle the database using objects)
    -----------------------------------------
    Spring Java Database Connectivity -> JDBC
    (we can use SQL queries at this level)
    ------------------------------------------
    Database Driver
    
    






































